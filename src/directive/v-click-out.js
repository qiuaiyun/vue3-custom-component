export default {
    mounted(el, binding) {
        const onClickOut = (event) => {
            if(!el.contains(event.target)) {
                binding.value(event)
            }
        }
        document.addEventListener('click', onClickOut);

        el._clickOutside = onClickOut;
    },
    unmounted(el) {
        document.removeEventListener('click', el._clickOutside);
        delete el._clickOutside;
    },
}