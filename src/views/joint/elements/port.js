const defaultOpt = {
    rankdir: 'left',
    text: '',
}
/**
 * 配置端口属性，不采用分组，需要指定端口元素位置
 * 
 * @returns { Object } 
 */
export const portConfig = (opt = { rankdir: 'left', text: 'port' }) => {
    const config = Object.assign(defaultOpt, {...opt})
    console.log(config);
    return {
        position: { name: config.rankdir },
        label: {
            position: {
                name: config.rankdir || 'left'
            },
            markup: [{
                tagName: 'text',
                selector: 'label'
            }]
        },
        attrs: {
            portBody: {
                magnet: true,
                width: 16,
                height: 16,
                x: -8,
                y: -8,
                fill:  '#D1D1D1',
                r: 6,
                stroke: "#E0218A",
                strokeWidth: 4 
            },
            label: {
                text: config.text
            }
        },
        markup: [{
            tagName: 'circle',
            selector: 'portBody'
        }],
    }
}

// 内端口配置，配合端口分组group使用，可自定义元素位置
export const portsIn = {
    attrs: {
        portBody: {
            magnet: true,
            // x: -8,
            // y: -8,
            fill:  '#D1D1D1',
            r: 6,
            stroke: "#E0218A",
            strokeWidth: 4  
        }
    },
    label: {
        position: { 
            name: 'left',
            args: { y: 0 }
        },
        markup: [{
            tagName: 'text',
            selector: 'label',
            className: 'label-text'
        }]
    },
    markup: [{
        tagName: 'circle',
        selector: 'portBody'
    }]
}

// 外端口配置，配合端口分组group使用，可自定义元素位置
export const portsOut = {
    position: { name: 'right' },
    attrs: {
        portBody: {
            magnet: true,
            fill:  '#D1D1D1',
            r: 6,
            stroke: "#E0218A",
            strokeWidth: 4
        }
    },
    label: {
        position: {
            name: 'right',
            args: { y: 0 }
        },
        markup: [{
            tagName: 'text',
            selector: 'label',
            className: 'label-text'
        }]
    },
    markup: [{
        tagName: 'circle',
        selector: 'portBody'
    }]
}