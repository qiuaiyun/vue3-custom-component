export default class ChildPlugin {
    constructor () {

    }
    calculateCost (distance) {
        console.log('calculateCost','Child');
        const cost = distance * 2; // Example logic for cost calculation
        console.log(`ChildPlugin: 行程:${distance} 公里，总结:${cost}元`);
        return Promise.resolve(cost);
    }
}