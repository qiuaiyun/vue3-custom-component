var defualtAttr = {
    id: `Node_${new Date().getTime()}${new Date().getMilliseconds()}`,
    fatherId: '',
    label: "新增名称", 
    width: 150, 
    height: 90, 
    isExpanded: true, 
    isActive: false,
    position: '新增职位',
    attr: {}
}

class Node {
    constructor(opt_ = {
        id: "",
        label: "新增名称", 
        width: 150, 
        height: 90, 
        isExpanded: true, 
        position: '新增职位',
        isActive: false
    }) {
        const cfg = Object.assign(defualtAttr, { ...opt_ })
        this.id = cfg.id
        this.fatherId = cfg.fatherId
        this.isExpanded = cfg.isExpanded
        this.isActive = cfg.isActive
        this.node = {
            label: cfg.label,
            width: cfg.width, 
            height: cfg.height, 
            position: cfg.position,
        }
        
        this.attr = cfg.attr
    }
}

export default Node;