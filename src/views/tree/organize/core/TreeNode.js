class TreeNode {
    constructor(data) {
        this.data = data;
        this.children = [];
        this.x = 0;
        this.y = 0;
    }
    init (treeData) {
        const root = this.buildTree(treeData);
        this.layoutTree(root);
        this.alignToSVG(root, 800); // Assuming SVG width is 800

        const result = treeToList(root);
    }
    buildTree(data) {
        const nodes = {};
        let root = null;
        
        data.forEach(item => {
            nodes[item.id] = new TreeNode(item);
        });
    
        data.forEach(item => {
            if(item.fatherId) {
                const parent = nodes[item.fatherId];
                parent.children.push(nodes[item.id]);
            } else {
                root = nodes[item.id];
            }
        });
    
        return root;
    }

    alignToSVG(root, svgWidth) {
        const rootX = root.x;
        const offsetX = svgWidth / 2 - rootX;
    
        function traverseAndAlign(node, offsetX) {
            node.x += offsetX;
            node.children.forEach(child => traverseAndAlign(child, offsetX));
        }
    
        traverseAndAlign(root, offsetX);
    }

    treeToList(root) {
        const result = [];
    
        function traverse(node, fatherId) {
            const { id, node: nodeInfo, attr } = node.data;
            result.push({
                id,
                fatherId,
                node: { ...nodeInfo, x: node.x, y: node.y },
                attr,
            });
            node.children.forEach(child => traverse(child, id));
        }
    
        traverse(root, '');
        return result;
    }
}