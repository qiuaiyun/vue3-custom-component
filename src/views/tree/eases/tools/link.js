/**
 * 绘制线条模式
 * 当前仅支持 curve曲线  straight直线
 * 
 * @param { String } name 曲线名称 curve || straight
 * @param { Array } links 连接线集合
 */
export const linkConnector = (name = "straight", links = []) => {
    let newLineSet = []
    if (name == void 0) return links
    if (!Array.isArray(links)) {
        new Error('The connection line must be an array of objects')
    }
    if (links.length == 0) return []
    
    let i = 0;
    while (i < links.length) {
        const { points } = links[i];
        let _D = '';
        // 二次贝塞尔曲线只支持一个控制点，起始两个坐标
        if (points.length === 3) {
            for(let k = 0; k < points.length; k++) {
                // straight 
                if (name === 'straight') {
                    if (k === 0) {
                        _D += `M ${points[k].x} ${points[k].y} `
                    } else {
                        _D += `L ${points[k].x} ${points[k].y} `
                    }
                }
                // curve 
                if (name === 'curve') {
                    if (k === 0) {
                        _D += `M ${points[k].x} ${points[k].y} `
                    } else if (k === 1){
                        _D += `Q ${points[k].x} ${points[k].y} `
                    } else {
                        _D += `${points[k].x} ${points[k].y}`
                    }
                }  
            }
        } 
        if (points.length === 5) {
            for (let j = 0; j < points.length; j++) {
                // 五个坐标绘制成线段L
                // if (j === 0) {
                //     _D += `M ${points[j].x} ${points[j].y} `
                // } else {
                //     _D += `L ${points[j].x} ${points[j].y} `
                // }

                // 五个点绘制会出现脱节问题
                // 测试绘制三次贝塞尔曲线，两个控制点，起点和终点
                if (j === 0) {
                    _D += `M${points[j].x},${points[j].y} `
                } else if (j === 1) {
                    _D += `C${points[j].x},${points[j].y} `
                } else if (j == 2) {
                    _D += `${points[j].x},${points[j].y} `
                } else if (j == 3) {
                    // _D += `${points[j].x},${points[j].y} `
                } else if (j == 4) {
                    _D += `${points[j].x},${points[j].y} `
                }

            }
            // console.log(_D, points, 212);
        }
        
        newLineSet.push(_D)
        i++;
    }
    
    return newLineSet
}