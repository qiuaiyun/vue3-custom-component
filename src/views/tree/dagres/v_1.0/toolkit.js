import Raphael from "raphael";

export const initDagreTree = ({ container="", model = null, nodes = [], links = [], connector = 'straight' }) => {
    const containerDom = document.querySelector(container);
    const { clientWidth, clientHeight } = containerDom;

    // 创建SVG画布
    const paper = new Raphael(containerDom, clientWidth, clientHeight)
    if (model) {
        // DagreJS图形的宽度和高度
        var graphWidth = model.graph().width;
        var graphHeight = model.graph().height;

        // 计算位移量
        var dx = (clientWidth - graphWidth) / 2;
        var dy = (clientHeight - graphHeight) / 2;

        // 设置viewBox保证内容完全展示
        const svg = paper.setViewBox(-dx, -dy, clientWidth, clientHeight)
    }

    // 创建rect节点
    // const rect = paper.rect(200, 100, 120, 100, 5)
    // const rectBox = rect.getBBox()
    // const { cx, cy, x:Rx, y:Ry, width, height }  = rectBox
    // 文本坐标x=rect.x + rect.width / 2
    // 文本坐标y=rect.y + rect.height / 2
    // let textX = Rx + width / 2, textY = Ry + height / 2;
    // console.log(rectBox, 'text');
    // const text = paper.text(textX, textY, '我是文本内容')
    // text.attr('font-size', 18)
    // text.attr('font-weight', '600')
    // console.log(containerDom, 3333);

    // 绘制元素
    let i = 0
    while (i < nodes.length) {
        // const rect = paper.rect(nodes[i].x, nodes[i].y, nodes[i].width, nodes[i].height, 5)
        // Paint布局（例如SVG和HTML）通常使用标准的盒子模型，元素的x和y位置从左上角开始计算。
        // 然而，DagreJS对位置的计算则略有不同，其x和y是基于元素的中心点进行的。
        // 需要转换元素x和y坐标才能对齐连线
        const rect = paper.rect(nodes[i].x - nodes[i].width / 2, nodes[i].y - nodes[i].height / 2, nodes[i].width, nodes[i].height, 5);
        const rectBox = rect.getBBox()
        const { cx, cy, x:Rx, y:Ry, width, height }  = rectBox
        let textX = Rx + width / 2, textY = Ry + height / 2;
        const text = paper.text(textX, textY, nodes[i].label)
        text.attr('font-size', 18)
        text.attr('font-weight', '600')
        i++;
    }

    // 获取连接线数据
    const newLinks = linkConnector(connector, links)
    let j = 0;
    while (j < newLinks.length) {
        const path = paper.path(newLinks[j])
            path.attr('stroke', "#10ac84")
            path.attr('stroke-width', 3)
        j++;
    }
    return paper
}

/**
 * 绘制线条模式
 * 当前仅支持 curve曲线  straight直线
 * 
 * @param { String } name 曲线名称 curve || straight
 * @param { Array } links 连接线集合
 */
export const linkConnector = (name = "straight", links = []) => {
    let newLineSet = []
    if (name == void 0) return links
    if (!Array.isArray(links)) {
        new Error('The connection line must be an array of objects')
    }
    if (links.length == 0) return []

    // while (j < links.length) {
    //     const { points } = links[j]
    //     // const path = paper.path(d.join(' '));
    //     // const path = paper.path(`M ${points[0].x} ${points[0].y} L ${points[1].x} ${points[1].y} L ${points[2].x} ${points[2].y}`)
    //     const path = paper.path(`M ${points[0].x} ${points[0].y} Q ${points[1].x} ${points[1].y} ${points[2].x} ${points[2].y}`)
    //         path.attr('stroke', "#10ac84")
    //         path.attr('stroke-width', 3)
    //     j++;
    // }
    
    let i = 0;
    while (i < links.length) {
        const { points } = links[i];
        let _D = '';
        for(let k = 0; k < points.length; k++) {
            // straight 
            if (name === 'straight') {
                if (k === 0) {
                    _D += `M ${points[k].x} ${points[k].y} `
                } else {
                    _D += `L ${points[k].x} ${points[k].y} `
                }
            }
            // curve 
            if (name === 'curve') {
                if (k === 0) {
                    _D += `M ${points[k].x} ${points[k].y} `
                } else if (k === 1){
                    _D += `Q ${points[k].x} ${points[k].y} `
                } else {
                    _D += `${points[k].x} ${points[k].y}`
                }
            }  
        }
        newLineSet.push(_D)
        i++;
    }
    
    return newLineSet
}