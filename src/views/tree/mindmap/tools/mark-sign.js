/**
 * draw is a '+' or '-' by coordinate
 * 
 * @param { Number } x
 * @param { Number } y
 * 
 *  const subtraction = `M${markerCoordinateX-5},${markerCoordinateY} L${markerCoordinateX+5},${markerCoordinateY}`
    const addition = `M${markerCoordinateX-5},${markerCoordinateY} L${markerCoordinateX+5},${markerCoordinateY} M${markerCoordinateX},${markerCoordinateY-5} L${markerCoordinateX},${markerCoordinateY+5}`
 */

export const subtraction = (x = 0, y = 0) => {
    return `M${x-5},${y} L${x+5},${y}`
}

export const addition = (x = 0, y = 0) => {
    return `M${x-5},${y} L${x+5},${y} M${x},${y-5} L${x},${y+5}`
}
