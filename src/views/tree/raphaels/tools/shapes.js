
/**
 * 元素
 * 
 * @param { RaphaelPaper } paper Raphael
 * @returns { Array<RaphaelElement> }
 */
export const createShape = (paper) => {
    if (paper) {
        return [
            paper.rect(300, 100, 120, 80, 5),
            paper.circle(200, 100, 40),
            paper.ellipse(200, 260, 50, 30)
        ]
    } else return []
}