/**
 * @author qiuny
 * @description 激活控件插件
 * @class
 * @see {@link CoverEditor}
 */

import { fabric } from "fabric";
import RotateIcon from "../assets/rotateIcon.svg";
import RomveIcon from "../assets/romveIcon";

class ActiveControlPlugin {
    constructor(canvas, editor) {
        this.canvas = canvas;
        this.editor = editor;
        this.transparentCorners = false;
        this.cornerColor = "#FFF";
        this.cornerStyle = "circle";
        this.cornerStrokeColor = "#0cc0df"
        this.borderColor = '#0cc0df'
        this.init();
    }

    init() {
        // 旋转图标
        rotationControl();
        // 删除图标
        // removeControl(this.canvas);

        fabric.Object.prototype.set({
            transparentCorners: false,
            borderColor: this.borderColor,
            cornerColor: this.cornerColor,
            borderScaleFactor: 2.5,
            cornerStyle: this.cornerStyle,
            cornerStrokeColor: this.cornerStrokeColor,
            borderOpacityWhenMoving: 1
        })
    }

    destroy() {
        console.log('ActiveControlPlugin destroy');
    }
}

// 删除图标
function removeControl(canvas) {
    function deleteObject(mouseEvent, target) {
        if (target.action === 'rotate') return true;
        const activeObject = canvas.getActiveObjects();
        if (activeObject) {
          activeObject.map((item) => canvas.remove(item));
          canvas.requestRenderAll();
          canvas.discardActiveObject();
        }
        return true;
    }
    
    function renderIcon(ctx, left, top, styleOverride, fabricObject) {
        const img = document.createElement("img");
        img.src = RomveIcon.icon1
        let size = this.cornerSize;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(img, -size / 2, -size / 2, size, size);
        ctx.restore();
    }

    fabric.Object.prototype.controls.deleteControl = new fabric.Control({
        x: 0.5,
        y: 0.5,
        offsetY: 16,
        offsetX: 16,
        cursorStyle: 'pointer',
        mouseUpHandler: deleteObject,
        render: renderIcon,
        cornerSize: 24
    })
}

// 旋转
function rotationControl() {
    const img = document.createElement("img");
    img.src = RotateIcon;
    function renderIconRotate(ctx, left, top, styleOverride, fabricObject) {
        drawImg(ctx, left, top, img, 40, 40, fabricObject.angle);
    }
    // 旋转图标
    fabric.Object.prototype.controls.mtr = new fabric.Control({
        x: 0,
        y: 0.5,
        cursorStyleHandler: fabric.controlsUtils.rotationStyleHandler,
        actionHandler: fabric.controlsUtils.rotationWithSnapping,
        offsetY: 30,
        // withConnecton: false,
        actionName: "rotate",
        render: renderIconRotate,
    });
}

function drawImg(ctx, left, top, img, wSize, hSize, angle) {
    if (angle === undefined) return;
    ctx.save();
    ctx.translate(left, top);
    ctx.rotate(fabric.util.degreesToRadians(angle));
    ctx.drawImage(img, -wSize / 2, -hSize / 2, wSize, hSize);
    ctx.restore();
}

export default ActiveControlPlugin;
