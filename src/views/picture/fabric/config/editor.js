export const config = {
    toolIcon: [
        // {
        //     icon: 'icon iconyidongbiaoge',
        //     type: 'Clear',
        //     textTip: '移动'
        // },
        {
            icon: 'icon iconyuanxing',
            type: 'Circle',
            textTip: '圆',
            isChecked: false
        },
        {
            icon: 'icon icontx-tuoyuanxing',
            type: 'Ellipse',
            textTip: '椭圆',
            isChecked: false
        },
        {
            icon: 'icon iconhuabi',
            type: 'PencilBrush',
            textTip: '铅笔画笔',
            isChecked: false
        },
        {
            icon: 'icon iconzhixianxianduan',
            type: 'Line',
            textTip: '线',
            isChecked: false
        },
        {
            icon: 'icon iconduobianxing',
            type: 'Polygon',
            textTip: '多边形',
            isChecked: false
        },
        {
            icon: 'icon iconjuxing',
            type: 'Rect',
            textTip: '矩形',
            isChecked: false
        },
        {
            icon: 'icon iconxingzhuang-sanjiaoxing',
            type: 'Triangle',
            textTip: '三角形',
            isChecked: false
        },
        {
            icon: 'icon iconsiweidaotu-zhexian',
            type: 'DrawPolyline',
            textTip: '自由折线|直线',
            isChecked: false
        },
        {
            icon: 'icon iconbuguizejuxing',
            type: 'DrawPolygon',
            textTip: '不规则形状',
            isChecked: false
        },
        {
            icon: 'icon iconjiantou',
            type: 'LineArrow',
            textTip: '线段箭头',
            isChecked: false
        },
        {
            icon: 'icon iconsoutheast-wind-0',
            type: 'LineSolidArrow',
            textTip: '线段实心箭头',
            isChecked: false
        },
        {
            icon: 'icon iconarrow_top',
            type: 'HosooArrow',
            textTip: '细尾箭头',
            isChecked: false
        },
    ],
    toolColor: [
        { 
            value: '#000000',
            textTip: '#000000'
        },
        { 
            value: '#2b4b82',
            textTip: '#2b4b82'
        },
        { 
            value: '#31356e',
            textTip: '#31356e'
        },
        { 
            value: '#94ddde',
            textTip: '#94ddde'
        },
        { 
            value: '#211951',
            textTip: '#211951'
        },
        { 
            value: '#836FFF',
            textTip: '#836FFF'
        },
        { 
            value: '#15F5BA',
            textTip: '#15F5BA'
        },
        { 
            value: '#F0F3FF',
            textTip: '#F0F3FF'
        },
        { 
            value: '#40A2E3',
            textTip: '#40A2E3'
        },
        { 
            value: '#FFF6E9',
            textTip: '#FFF6E9'
        },
        { 
            value: '#BBE2EC',
            textTip: '#BBE2EC'
        },
        { 
            value: '#0D9276',
            textTip: '#0D9276'
        },
        { 
            value: '#1F2544',
            textTip: '#1F2544'
        },
        { 
            value: '#474F7A',
            textTip: '#474F7A'
        },
        { 
            value: '#81689D',
            textTip: '#81689D'
        },
        { 
            value: '#FFD0EC',
            textTip: '#FFD0EC'
        },
        { 
            value: '#153448',
            textTip: '#153448'
        },
        { 
            value: '#3C5B6F',
            textTip: '#3C5B6F'
        },
        { 
            value: '#948979',
            textTip: '#948979'
        },
        { 
            value: '#DFD0B8',
            textTip: '#DFD0B8'
        },
        { 
            value: '#FE7A36',
            textTip: '#FE7A36'
        },
        { 
            value: '#3652AD',
            textTip: '#3652AD'
        },
        { 
            value: '#280274',
            textTip: '#280274'
        },
        { 
            value: '#E9F6FF',
            textTip: '#E9F6FF'
        },
    ],
    toolText: [
        {
            icon: 'icon iconwenben',
            type: 'IText',
            textTip: '文本'
        },
        {
            icon: 'icon iconshupaiwenzi',
            type: 'Textbox',
            textTip: '竖排文本'
        },
    ],
    // 参照类 TextOptions
    fontStyles: [
        {
            icon: 'icon iconzitijiacu',
            value: 'bold',
            textTip: '加粗',
            key: 'fontWeight',
            isActive: false
        },
        {
            icon: 'icon iconqingxie',
            value: 'italic',
            textTip: '倾斜',
            key: 'fontStyle',
            isActive: false
        },
        {
            icon: 'icon iconzitixiahuaxian',
            value: true,
            textTip: '下划线',
            key: 'underline',
            isActive: false
        }
    ],
    horizontalAlignment: [
        {
            // 左对齐
            icon: 'icon iconzuoduiqi',
            textTip: '左对齐',
            key: 'left',
            originX: 'left'
        },
        {
            // 居中对齐
            icon: 'icon iconchuizhijuzhongduiqi',
            textTip: '居中对齐',
            key: 'left',
            originX: 'center'
        },
        {
            // 右对齐
            icon: 'icon iconyouduiqi',
            textTip: '居中对齐',
            key: 'left',
            originX: 'right'
        }
    ],
    verticalAlignment: [
        {
            // 顶对齐
            icon: 'icon icondingbuduiqi',
            textTip: '顶对齐',
            key: 'top',
            originY: 'top'
        },
        {
            // 垂直居中对齐
            icon: 'icon iconshuipingjuzhongduiqi',
            textTip: '垂直居中对齐',
            key: 'top',
            originY: 'center'
        },
        {
            // 低对齐
            icon: 'icon icondibuduiqi',
            textTip: '低对齐',
            key: 'top',
            originY: 'bottom'
        }
    ],
    // 形状默认参数
    defParams: {
        fill: '#BEBEBE',
        stroke: '#31356e',
        strokeWidth: 0
    },
    // 默认画布大小
    artbordSize: {
        width: 900,
        height: 1200,
    }
}