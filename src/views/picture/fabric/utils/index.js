import { v4 as uuidV4 } from 'uuid'
import { useFileDialog } from '@vueuse/core'


/**
 * Polygon 多边形边和圆角计算
 * 
 * @param { Number } sideCount 边数
 * @param { Number } radius 圆角值
 * @returns 
 */
export const regularPolygonPoints = (sideCount, radius) => {
    var sweep = Math.PI * 2 / sideCount;
    var cx = radius;
    var cy = radius;
    var points = [];
    for (var i = 0; i < sideCount; i++) {
        var x = cx + radius * Math.cos(i * sweep);
        var y = cy + radius * Math.sin(i * sweep);
        points.push({
            x: x,
            y: y
        });
    }
    return (points);
}

/**
 * Line 
 * 
 * @param { number } x1 
 * @param { number } y1 
 * @param { number } x2 
 * @param { number } y2 
 */
export const linePoints = (x=100, y=200, distance = 100) => {
    const x1 = x, y1 = y, x2 = Math.ceil(x+distance), y2 = y;
    return [x1, y1, x2, y2]
}

/**
 * 下载文件
 * 
 * @param { Stirng } fileUrl 文件下载地址
 * @param { String } fileType 文件类型
 */
export const downloadFile = (fileUrl, fileType) => {
    const A = document.createElement('a')
    A.href = fileUrl
    A.download = `${timeStamp()}.${fileType}`
    document.body.appendChild(A)
    A.click()
    A.remove()
}

export const timeStamp = () => {
    var date = new Date()
    var y = date.getFullYear()
    var m = date.getMonth()+1
    var d = date.getDate()
    var hour = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds()
    return `${y}${m}${d}_${hour}${minutes}${seconds}`
}

/**
 * 选择文件过滤，根据不同类型加载不同文件
 * 
 * @see {@link useFileDialog}  @vueuse/core
 * @param { Object } options 
 * @param { Stirng } multiple
 * @param { Stirng } capture Select the input source for the capture file.
 * @param { Stirng } reset Reset when open file dialog.
 * @param { Stirng } accept 
 */
export const chooseFileByType = (options = {
    multiple:true,
    capture: '', 
    reset: false,
    accept: '*'
}) => {
    return new Promise((resolve, reject) => {
        const { files, open, reset, onChange } = useFileDialog({...options})
        onChange(function(files) {
            resolve(files)
        })
        open()
        if (options.reset) {
            reset()
        }
    })
}