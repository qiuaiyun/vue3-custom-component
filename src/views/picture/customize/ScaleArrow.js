import { fabric } from "fabric";

fabric.ScaleArrow = fabric.util.createClass(fabric.Line, {
    type: "ScaleArrow",
    initialize: function (points, options) {
        options || (options = {})
        // 配置当前对象附件属性
        this.set({
            arrowLen:options.arrowLen || 35,
            hasControls: false, // 移除控制框
            hasBorders: false,  // 移除边界框
            selectable: false,   // 禁用选中状态
        })
        // 传递参数options
        this.callSuper('initialize', points, options)
    },
    _render: function(ctx) {
        this.callSuper('_render', ctx)
        // 重新计算给定宽度和高度的线点
        const { x1, y1, x2, y2 } = this.calcLinePoints();
        /**
         * 计算原点(0,0)到(x,y)点的线段与x轴正方向之间的平面角度 (弧度值)
         * @see {@link https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/atan2}
         */
        const angle = Math.atan2(y2 - y1, x2 - x1);
        // 设置箭头长度 50
        const headLen = this.arrowLen
        // 计算并绘制箭头头部左侧，逆时针-30度
        const leftX = x2 - headLen * Math.cos(angle - Math.PI / 6)
        const leftY = y2 - headLen * Math.sin(angle - Math.PI / 6)
        // 计算并绘制箭头头部右侧，顺时针+30度
        const rightX = x2 - headLen * Math.cos(angle + Math.PI / 6)
        const rightY = y2 - headLen * Math.sin(angle + Math.PI / 6)
        // headlen-5 得差值，箭头两侧垂直中心线，也可以不用怎么做
        // 绘制箭头左侧垂直线段中心点坐标，-15度
        const centerLeftX = x2 - (headLen-5) * Math.cos(angle - Math.PI / 12)
        const centerLeftY = y2 - (headLen-5) * Math.sin(angle - Math.PI / 12)
        // 绘制箭头右侧垂直线段中心点坐标，+15度
        const centerRightX = x2 - (headLen-5) * Math.cos(angle + Math.PI / 12)
        const centerRightY = y2 - (headLen-5) * Math.sin(angle + Math.PI / 12)

        ctx.save()
        ctx.beginPath();
        // moveTo表示重新开始画点，lineTo表示连续画点
        ctx.moveTo(x1, y1)
        // 绘制左侧中点
        ctx.lineTo(centerLeftX, centerLeftY);
        // 绘制左侧点
        ctx.lineTo(leftX, leftY);
        // 绘制终点
        ctx.lineTo(x2, y2);
        // 绘制右侧点
        ctx.lineTo(rightX, rightY);
        // 绘制右侧中点
        ctx.lineTo(centerRightX, centerRightY);
        // 回到起点
        // ctx.lineTo(startX, startY);
        ctx.closePath()

        ctx.fillStyle = this.fill || "#FDFFE2";
        ctx.fill()
        // 边框线
        ctx.strokeStyle = this.stroke || "#83B4FF";
        ctx.lineWidth = this.lineWidth || 5
        ctx.stroke();
        ctx.restore();

        // 辅助点，帮助标识坐标点，很好用
        // drawHelperCircle(x1, y1, 'blue')
        // drawHelperCircle(x2, y2, 'tomato')
        function drawHelperCircle(x, y, fill="#10ac84") {
            ctx.beginPath();
            // 绘制一个半径为3的圆
            ctx.arc(x, y, 5, 0, Math.PI * 2);
            // 设置辅助圆的颜色
            ctx.fillStyle = fill;
            ctx.fill();
            ctx.closePath();
        }
    }
})

export default fabric.ScaleArrow;