import { v4 as uuidV4 } from "uuid";
export default class Node {
    constructor(
        uniqueId = uuidV4().replace(/-/g, ""),
        label = "新增节点",
        icon = "ios-archive",
        color = "#10AC84",
        pos = { left: 50, top: 50 },
        isAction = false
    ) {
        this.id = uniqueId;
        this.label = label;
        this.icon = icon;
        this.color = color;
        this.left = pos.left;
        this.top = pos.top;
        this.isAction = isAction
    }
}
