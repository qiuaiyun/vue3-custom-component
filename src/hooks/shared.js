export default function useSharedCore() {
    const fabric = inject("fabric");
    const coverEditor = inject("coverEditor");
    const mixinState = inject("mixinState");

    return {
        fabric,
        coverEditor,
        mixinState,
    };
}
