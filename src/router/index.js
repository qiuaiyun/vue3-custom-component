import { createRouter, createWebHistory } from 'vue-router';
import Layout  from '@/components/layout/index.vue';
import testRouter from './test';
import { isEnbleTestRouter } from '@/scripts/config';
import { beforeNavRouteGuard, afterNavRouteGuard } from "./router-guard";

const routes = [
    {
        path: '/',
        component: Layout,
        redirect: '/home',
        children: [
            {
                path: '/home',
                name: 'home',
                component: () => import('@/views/home/index.vue'),
                meta: { title: '首页',  hidden: true }
            },
            {
                path: '/picture',
                name: 'picture',
                component: () => import('@/views/picture/index.vue'),
                meta: { title: 'fabric模块', hidden: false },
                children: [
                    {
                        path: '/picture/customize',
                        name: 'customize',
                        component: () => import('@/views/picture/customize/index.vue'),
                        meta: { title: '自定义实践'},
                    },
                    {
                        path: '/picture/drawpath',
                        name: 'drawpath',
                        component: () => import('@/views/picture/drawpath/index.vue'),
                        meta: { title: '画笔功能'},
                    },
                    {
                        path: '/picture/fabric',
                        name: 'fabric',
                        component: () => import('@/views/picture/fabric/index.vue'),
                        meta: { title: '封面编辑器'},
                    }
                ]
            },
            {
                path: '/editors',
                name: 'editors',
                component: () => import('@/views/editors/index.vue'),
                meta: { title: '富文本编辑器' },
                children: [
                    {
                        path: '/editors/tiptaps',
                        name: 'tiptaps',
                        component: () => import('@/views/editors/tiptaps/index.vue'),
                        meta: { title: 'tiptap富文本', hidden: false },
                    }
                ]
            },
            {
                path: '/toolkit',
                name: 'toolkit',
                component: () => import('@/views/toolkit/index.vue'),
                meta: { title: '工具类', hidden: false },
                children: [
                    {
                        path: '/toolkit/camera',
                        name: 'toolkitcamera',
                        component: () => import('@/views/toolkit/camera/index.vue'),
                        meta: { title: '获取摄像头'},
                    },
                    {
                        path: '/toolkit/signature',
                        name: 'toolkitsignature',
                        component: () => import('@/views/toolkit/signature/index.vue'),
                        meta: { title: '签名画板'},
                    },
                    {
                        path: '/toolkit/tooltip',
                        name: 'toolkittooltip',
                        component: () => import('@/views/toolkit/tooltip/index.vue'),
                        meta: { title: 'tooltip提示'},
                    }
                ]
            },
            {
                path: '/scan',
                name: 'scan',
                component: () => import('@/views/scan/index.vue'),
                meta: { title: '扫描动画', hidden: false },
                children: [
                    {
                        path: '/scan/base',
                        name: 'scan-base',
                        component: () => import('@/views/scan/base/index.vue'),
                        meta: { title: '基础案例', hidden: false },
                    },
                    {
                        path: '/scan/custom',
                        name: 'scan-custom',
                        component: () => import('@/views/scan/custom/index.vue'),
                        meta: { title: '高级玩法', hidden: false },
                    }
                ]
            },
            {
                path: '/tree',
                name: 'tree',
                component: () => import('@/views/tree/index.vue'),
                meta: { title: 'svg树案例', hidden: false },
                children: [
                    {
                        path: '/tree/organize',
                        name: 'organize',
                        component: () => import('@/views/tree/organize/index.vue'),
                        meta: { title: 'Carrot导图', hidden: false },
                    },
                    {
                        path: '/tree/city',
                        name: 'city',
                        component: () => import('@/views/tree/city/index.vue'),
                        meta: { title: '城市树', hidden: false },
                    },
                    {
                        path: '/tree/raphaels',
                        name: 'raphaels',
                        component: () => import('@/views/tree/raphaels/index.vue'),
                        meta: { title: 'raphael基本用法', hidden: false },
                    },
                    {
                        path: '/tree/small',
                        name: 'small',
                        component: () => import('@/views/tree/small/index.vue'),
                        meta: { title: '小墨图自定义曲线', hidden: false },
                    },
                    {
                        path: '/tree/eases',
                        name: 'eases',
                        component: () => import('@/views/tree/eases/index.vue'),
                        meta: { title: 'dagre各类布局', hidden: false },
                    },
                    {
                        path: '/tree/graphs',
                        name: 'graphs',
                        component: () => import('@/views/tree/graphs/index.vue'),
                        meta: { title: 'graphs基本用法', hidden: false },
                    },
                    {
                        path: '/tree/dagres',
                        name: 'dagres',
                        component: () => import('@/views/tree/dagres/index.vue'),
                        meta: { title: 'dagre基本用法', hidden: false},
                    },
                    {
                        path: '/tree/undirected',
                        name: 'undirected',
                        component: () => import('@/views/tree/undirected/index.vue'),
                        meta: { title: 'graph无向图', hidden: false },
                    },
                    {
                        path: '/tree/mindmap',
                        name: 'mindmap',
                        component: () => import('@/views/tree/mindmap/index.vue'),
                        meta: { title: 'Bruce思维导图', hidden: false },
                    },
                ]
            },
            {
                path: '/drag',
                name: 'drag',
                component: () => import('@/views/drag/index.vue'),
                meta: { title: 'drag拖拽案例', hidden: false },
                children: [
                    {
                        path: '/drag/file',
                        name: 'drag-file',
                        component: () => import('@/views/drag/file/index.vue'),
                        meta: { title: '批量拖拽效果', hidden: false },
                    },
                    {
                        path: '/drag/interacts',
                        name: 'drag-interacts',
                        component: () => import('@/views/drag/interacts/index.vue'),
                        meta: { title: 'interact基本用法', hidden: false },
                    },
                    {
                        path: '/drag/list',
                        name: 'drag-list',
                        component: () => import('@/views/drag/list/index.vue'),
                        meta: { title: '单列表拖拽', hidden: false },
                    },
                    {
                        path: '/drag/related',
                        name: 'drag-related',
                        component: () => import('@/views/drag/related/index.vue'),
                        meta: { title: '多列表拖拽', hidden: false },
                    }
                ]
            },
            {
                path: '/joint',
                name: 'joint',
                component: () => import('@/views/joint/index.vue'),
                meta: { title: 'jointjs案例', hidden: false },
                children: [
                    {
                        path: '/joint/elements',
                        name: 'joint-elements',
                        component: () => import('@/views/joint/elements/index.vue'),
                        meta: { title: '元素', hidden: false },
                    },
                    {
                        path: '/joint/line',
                        name: 'joint-line',
                        component: () => import('@/views/joint/line/index.vue'),
                        meta: { title: '连线', hidden: false },
                    },
                    {
                        path: '/joint/events',
                        name: 'joint-events',
                        component: () => import('@/views/joint/events/index.vue'),
                        meta: { title: '事件', hidden: false },
                    },
                    {
                        path: '/joint/tools',
                        name: 'joint-tools',
                        component: () => import('@/views/joint/tools/index.vue'),
                        meta: { title: '元素工具', hidden: false },
                    },
                    {
                        path: '/joint/mindmap',
                        name: 'joint-mindmap',
                        component: () => import('@/views/joint/mindmap/index.vue'),
                        meta: { title: '思维导图', hidden: false},
                    },
                    {
                        path: '/joint/ports',
                        name: 'joint-ports',
                        component: () => import('@/views/joint/ports/index.vue'),
                        meta: { title: '端口锚点', hidden: false},
                    },
                    {
                        path: '/joint/flowengine',
                        name: 'joint-flowengine',
                        component: () => import('@/views/joint/flowengine/index.vue'),
                        meta: { title: '流程设计引擎', hidden: false},
                    },
                ]
            },
            {
                path: '/jsplumbs',
                name: 'jsplumbs',
                component: () => import('@/views/jsplumbs/index.vue'),
                meta: { title: 'jsplumbs案例', hidden: false },
                children: [
                    {
                        path: '/jsplumbs/demo',
                        name: 'jsplumbs-demo',
                        component: () => import('@/views/jsplumbs/demo/index.vue'),
                        meta: { title: '默认连线', hidden: false },
                    },
                    {
                        path: '/jsplumbs/drag',
                        name: 'jsplumbs-drag',
                        component: () => import('@/views/jsplumbs/drag/index.vue'),
                        meta: { title: '拖拽连线', hidden: false },
                    },
                    {
                        path: '/jsplumbs/auto',
                        name: 'jsplumbs-auto',
                        component: () => import('@/views/jsplumbs/auto/index.vue'),
                        meta: { title: '自动连线', hidden: false},
                    },
                    {
                        path: '/jsplumbs/flow',
                        name: 'jsplumbs-flow',
                        component: () => import('@/views/jsplumbs/flow/index.vue'),
                        meta: { title: '流程图', hidden: false },
                    }
                ]
            },
            {
                path: '/ui',
                name: 'ui',
                component: () => import('@/views/ui/index.vue'),
                meta: { title: 'UI组件',  hidden: false },
                children: [
                    {
                        path: '/ui/scroll',
                        name: 'scroll',
                        component: () => import('@/views/ui/scroll/index.vue'),
                        meta: { title: '无限滚动列表', hidden: false },
                    },
                    {
                        path: '/ui/viewer-image',
                        name: 'viewer-image',
                        component: () => import('@/views/ui/viewer-image/index.vue'),
                        meta: { title: '图片预览器', hidden: false },
                    },
                    {
                        path: '/ui/transfer',
                        name: 'transfer',
                        component: () => import('@/views/ui/transfer/index.vue'),
                        meta: { title: '穿梭框', hidden: false },
                    },
                    {
                        path: '/ui/flip',
                        name: 'flip',
                        component: () => import('@/views/ui/flip/index.vue'),
                        meta: { title: 'flip动画', hidden: false },
                    },
                    {
                        path: '/ui/popper',
                        name: 'popper',
                        component: () => import('@/views/ui/popper/index.vue'),
                        meta: { title: '下拉面板', hidden: false },
                    },
                    {
                        path: '/ui/tooltip',
                        name: 'tooltip',
                        component: () => import('@/views/ui/tooltip/index.vue'),
                        meta: { title: '工具提示', hidden: false },
                    }
                ]
            },
            {
                path: '/bscrolls',
                name: 'bscrolls',
                component: () => import('@/views/bscrolls/index.vue'),
                meta: { title: 'BetterScroll', hidden: false },
                redirect: '/pulldowns/demo',
                children: [
                    {
                        path: '/bscrolls/demo',
                        name: 'demo',
                        component: () => import('@/views/bscrolls/demo/index.vue'),
                        meta: { title: '纵向演示', hidden: false},
                    },
                    {
                        path: '/bscrolls/horizontal',
                        name: 'horizontal',
                        component: () => import('@/views/bscrolls/horizontal/index.vue'),
                        meta: { title: '横向演示', hidden: false},
                    }
                ]
            },
            {
                path: '/:pathMatch(.*)*',
                meta: { title: '错误页面', hidden: true },
                component: () => import('@/views/error/index.vue')
            }        
        ]
    },
]

isEnbleTestRouter && routes[0].children.unshift(testRouter)

const router = createRouter({
    history: createWebHistory(),
    routes
})
// 全局前置守卫
router.beforeEach(beforeNavRouteGuard)

// 全局后置守卫
router.afterEach(afterNavRouteGuard)

export default router;
