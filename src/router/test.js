const testRouter = {
    path: '/test',
    name: 'test',
    component: () => import('@/views/test/index.vue'),
    meta: { title: 'VUE3新特性' },
    children: [
        {
            path: '/test/funs',
            name: 'funs',
            component: () => import('@/views/test/funs/index.vue'),
            meta: { title: 'VUE3新功能', hidden: false },
        }
    ]
}

export default testRouter;