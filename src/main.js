import { createApp } from "vue";
import { createPinia } from 'pinia';
import App from "./App.vue";
import router from "./router";
import '@/assets/index.scss';
import '@/assets/styles/font.scss';
import VClickOut from "./directive/v-click-out";
import ImagesViewerVue3 from 'images-viewer-vue3';


// view组件库样式会影响primevue
import {
    Button,
    Table,
    DropdownItem,
    DropdownMenu,
    Dropdown,
    Icon,
    Menu,
    MenuItem,
    Submenu,
    RadioGroup,
    Radio,
    Input,
    Col,
    Row,
    Switch,
    ColorPicker,
    Drawer,
    Tooltip
} from "view-ui-plus";
import "view-ui-plus/dist/styles/viewuiplus.css";

// naive ui组件库
// import { create, NButton } from 'naive-ui'
// const NaiveUi = create({
//     components: [NButton]
// })
const pinia = createPinia()
const app = createApp(App);
app.directive("click-out", VClickOut);

// 挂载路由
app.use(pinia);
app.use(router);
app.use(ImagesViewerVue3)
// app.use(ViewUIPlus)
// app.use(NaiveUi)
app.component("Button", Button);
app.component("Table", Table);
app.component("Dropdown", Dropdown);
app.component("DropdownMenu", DropdownMenu);
app.component("DropdownItem", DropdownItem);
app.component("Icon", Icon);
app.component("Menu", Menu);
app.component("MenuItem", MenuItem);
app.component("Submenu", Submenu);
app.component("RadioGroup", RadioGroup);
app.component("Radio", Radio);
app.component("Input", Input);
app.component("Col", Col);
app.component("Row", Row);
app.component("Switch", Switch);
app.component("ColorPicker", ColorPicker);
app.component("Drawer", Drawer);
app.component("Tooltip", Tooltip);

app.mount("#app");
